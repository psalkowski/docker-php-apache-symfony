FROM php:7.4-apache

ADD ./vhost.conf /etc/apache2/sites-enabled/000-default.conf

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php -r "if (hash_file('sha384', 'composer-setup.php') === 'e0012edf3e80b6978849f5eff0d4b4e4c79ff1609dd1e613307e16318854d24ae64f26d17af3ef0bf7cfb710ca74755a') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php
RUN php -r "unlink('composer-setup.php');"
RUN mv composer.phar /usr/local/bin/composer
RUN composer config -g github-oauth.github.com f410a4f07298fda97aaca1dc7aa7ca70b4fdc455

RUN curl -s https://get.symfony.com/cli/installer | bash
RUN mv /root/.symfony/bin/symfony /usr/local/bin/symfony

RUN a2enmod rewrite

RUN apt update \
    && apt install -y git zip unzip \
    && apt clean \
    && rm -rf /var/lib/apt/lists/

RUN pecl install xdebug-2.8.1 \
    && docker-php-ext-enable xdebug \
    && docker-php-ext-install pdo_mysql

EXPOSE 80
WORKDIR /project
